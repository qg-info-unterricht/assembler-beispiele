section .data
even_msg  db  'Gerade Zahl!' 
len1  equ  $ - even_msg 
   
odd_msg db  'Ungerade Zahl!'   
len2  equ  $ - odd_msg

userMsg db 'Bitte Zahl eingeben: ' ; Eingabeprompt
lenUserMsg equ $-userMsg    

section .bss           
    antwort resb 1

section .text
    global _start            
	
_start:                     

    ;User prompt ausgeben
    mov eax, 4
    mov ebx, 0
    mov ecx, userMsg
    mov edx, lenUserMsg
    int 80h

    ;Benutzereingabe lesen
    mov eax, 3       ; syscall Nummer: 3
    mov ebx, 0       ; lesen von stdin
    mov ecx, antwort  ; antwort adresse in ecx
    mov edx, 1        ; 5 bytes lang
    int 80h           ; Syscall!

    mov   ax, [antwort]          

    and   ax, 1              ;and ax with 1
    jz    evnn
   
   
   mov   eax, 4             ;system call number (sys_write)
   mov   ebx, 1             ;file descriptor (stdout)
   mov   ecx, odd_msg       ;message to write
   mov   edx, len2          ;length of message
   int   0x80               ;call kernel
   jmp   exitprog

evnn:   
  
   mov   ah,  09h
   mov   eax, 4             ;system call number (sys_write)
   mov   ebx, 1             ;file descriptor (stdout)
   mov   ecx, even_msg      ;message to write
   mov   edx, len1          ;length of message
   int   0x80               ;call kernel

exitprog:

   mov   eax,1              ;system call number (sys_exit)
   int   0x80               ;call kernel:
