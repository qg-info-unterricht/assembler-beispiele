section .data
even_msg  db  'Gerade Zahl!' 
len1  equ  $ - even_msg 
   
odd_msg db  'Ungerade Zahl!'   
len2  equ  $ - odd_msg

section .text
   global _start            
	
_start:                     ;tell linker entry point
   mov   ax, 9h           ;getting 8 in the ax 
   and   ax, 1              ;and ax with 1
   jz    evnn
   
   
   mov   eax, 4             ;system call number (sys_write)
   mov   ebx, 1             ;file descriptor (stdout)
   mov   ecx, odd_msg       ;message to write
   mov   edx, len2          ;length of message
   int   0x80               ;call kernel
   jmp   exitprog

evnn:   
  
   mov   ah,  09h
   mov   eax, 4             ;system call number (sys_write)
   mov   ebx, 1             ;file descriptor (stdout)
   mov   ecx, even_msg      ;message to write
   mov   edx, len1          ;length of message
   int   0x80               ;call kernel

exitprog:

   mov   eax,1              ;system call number (sys_exit)
   int   0x80               ;call kernel:
