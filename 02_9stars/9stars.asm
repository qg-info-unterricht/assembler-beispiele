section	.data
    msg db 'Displaying 9 stars',0xa ;Message, Aneinanderreihung von "byte" (db) Bereichen
    len equ $ - msg  ;Laenge der Message
    s2  times 9 db '*' ; 9 x ein byte (db) mit dem Inhalt "*"


section	.text
   global _start	 ;must be declared for linker
	
_start:	         ;startadresse
   mov	edx,len  ;Laenge nach edx
   mov	ecx,msg  ;Message nach ecx
   mov	ebx,1    ;file descriptor (stdout)
   mov	eax,4    ;system call number (sys_write)
   int	0x80     ;call kernel
	
   mov	edx,9    ;message length
   mov	ecx,s2   ;message to write
   mov	ebx,1    ;file descriptor (stdout)
   mov	eax,4    ;system call number (sys_write)
   int	0x80     ;call kernel
	
   mov	eax,1    ;system call number (sys_exit)
   int	0x80     ;call kernel
	
