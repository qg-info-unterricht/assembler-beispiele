section .data                           ;.data Abschnitt
   userMsg db 'Bitte Zahl eingeben: ' ; Eingabeprompt
   lenUserMsg equ $-userMsg           ; Laenge 
   dispMsg db 'Du hast die folgende Zahl eingegeben: '    ; Ausgabeprompt
   lenDispMsg equ $-dispMsg           ; Laenge      

section .bss           ; Beschreibbarer Speicher, vobelegt mit 0-en
   antwort resb 5      ; 5 Byte 
	
section .text          ; coe Abschnitt
   global _start
	
_start:                

   ;User prompt ausgeben
   mov eax, 4
   mov ebx, 0   
   mov ecx, userMsg
   mov edx, lenUserMsg
   int 80h

   ;Neu: Benutzereingabe lesen
   mov eax, 3       ; syscall Nummer: 3
   mov ebx, 0       ; lesen von stdin
   mov ecx, antwort  ; antwort adresse in ecx
   mov edx, 5        ; 5 bytes lang
   int 80h           ; Syscall!
	
   ;Ausgabe
   mov eax, 4
   mov ebx, 0
   mov ecx, dispMsg
   mov edx, lenDispMsg
   int 80h  

   ;Ausgabe der eingegebenen Zahl
   mov eax, 4
   mov ebx, 0 
   mov ecx, antwort
   mov edx, 5
   int 80h  
    
   ; Exit code
   mov eax, 1
   mov ebx, 0
   int 80h
