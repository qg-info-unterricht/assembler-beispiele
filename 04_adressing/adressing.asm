section	.data
tabelle TIMES 10 DW 97

section	.text
   global _start     ;must be declared for linker (ld)
_start:             ;tell linker entry point

 
   MOV ECX, 98
   MOV EBX, tabelle
   MOV [EBX + 2], ECX
   INC ECX
   INC ECX
   ADD EBX,8 
   MOV [EBX], ECX
	
   ;tabelle ausgeben
   mov	edx,20     ;message length
   mov	ecx,tabelle   ;message to write
   mov	ebx,1       ;file descriptor (stdout)
   mov	eax,4       ;system call number (sys_write)
   int	0x80        ;call kernel

   mov	eax,1       ;system call number (sys_exit)
   int	0x80        ;call kernel

