; Hello World in Assembler
; 
; Uebersetzen:
; nasm -f elf hello.asm
; ld -m elf_i386 -s -o hello hello.o

; Speichervariablen 
SECTION .data
msg:    db  "Hello World!",10 ; db - 1 Byte
                              ; 10 - Newline

len:    equ $-msg             ; Berechnung der Laenge der 
                              ; Speichervariablen msg
                              ; benoetigt für die Ausgabe 
                              ; Im Beispiel 13

global _start ; Label fuer den Programmstart

SECTION .text

_start:
; Initialisieren und ausfuehren des
; Linux syscalls fuer Ausgabe an STDOUT
 mov eax,4    ; Syscall-ID (4 = sys_write)
 mov ebx,1    ; Ausgabe-Descriptor (1 = stdout)
 mov ecx,msg  ; Adresse des ersten Zeichens (msg)
 mov edx,len  ; Laenge der Zeichenkette (berechnet als len)
 int 80h      ; Softwareinterrupt 0x80, um den Syscall 
              ; (write(1,msg,len)) auszufuehren
; Programm beenden
 mov eax,1    ; Syscall-ID (1 = exit)
 mov ebx,0    ; Rueckgabewert 0 (Alles in Ordnung)
 int 80h      ; Softwareinterrupt 0x80, um den Syscall
              ; auszuführen



